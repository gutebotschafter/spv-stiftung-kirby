#!/usr/bin/env bash

DOCKER_PHP=$(docker ps | grep _php_ | cut -d' ' -f1)
DOCKER_NGINX=$(docker ps | grep _web_ | cut -d' ' -f1)

echo "Copying local config"
rm -f site/config/config.php
cp site/config/config-local.php site/config/config.php
echo ""

echo "Installing npm dependencies..."
docker exec -it ${DOCKER_NGINX} sh -c "cd /var/www/html && npm install"
echo ""

echo "Installing composer dependencies"
docker exec -it ${DOCKER_PHP} sh -c "cd /var/www/html && php composer.phar install"
echo

echo "Installing Updater Client"
git clone git@bitbucket.org:gutebotschafter/kirby-updater-client.git
mv kirby-updater-client/updater.php ./updater.php
rm -rf kirby-updater-client
echo ""

echo "All done! You can now execute ./webpack-dev-server.sh"
echo ""
