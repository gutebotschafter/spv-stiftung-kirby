<?php

$global = include(__DIR__ . "/config-global.php");

$configuration = [
    'debug'  => false,
    'cache'  => true,
    'locale' => 'de_DE.utf-8',
    'mgfagency.twig.debug' => false,
    'mgfagency.twig.cache' => true,
    'email' => [
        'transport' => [
            'type' => 'smtp',
            'host' => 's',
            'port' => 587,
            'auth' => true,
            'security' => 'tls',
            'username' => '',
            'password' => ''
        ]
    ]
];

$complete = array_merge($global, $configuration);

return $complete;
