<?php

return [
    'locale' => 'de_DE.utf-8',
    'mgfagency.twig.function.inlineRenderer' => 'inlineRenderer',
    "token" => "",
    "environments" => [
        "production" => "https://awesome-site.de",
        "stage" => "https://stage.awesome-site.de",
        "virt" => "http://localhost"
    ]
];
