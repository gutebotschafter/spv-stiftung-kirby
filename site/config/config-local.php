<?php

$global = include(__DIR__ . "/config-global.php");

$configuration = [
    'debug'  => true,
    'cache'  => false,
    'locale' => 'de_DE.utf-8',
    'mgfagency.twig.debug' => true,
    'mgfagency.twig.cache' => false,
    'email' => [
        'transport' => [
            'type' => 'smtp',
            'host' => 's',
            'port' => 587,
            'auth' => true,
            'security' => 'tls',
            'username' => '',
            'password' => ''
        ]
    ]
];

$complete = array_merge($global, $configuration);

return $complete;
