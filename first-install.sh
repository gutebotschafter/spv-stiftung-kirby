#!/usr/bin/env bash

echo "Removes old git..."
rm -rf .git
echo

echo "Initialize Git..."
echo
git init
echo

chmod +x post-install.sh
./post-install.sh

rm -f first-install.sh
