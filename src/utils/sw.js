const { assets } = global.serviceWorkerOption;

const OFFLINE_URL = new URL('/pwa/offline.html', global.location).toString();
const CACHE_REGEX = /\/(assets|images|pwa)\//;
const CACHE_NAME = webpack.env.CACHE_KEY;
const URLS_TO_CACHE = [global.location.href].concat(
    assets.map(path => {
        return new URL(`/assets${path}`, global.location).toString();
    })
);
const BLACKLIST_ROUTES = [
    'panel'
];

// œTODO: Fix it
// URLS_TO_CACHE.push(OFFLINE_URL);

self.addEventListener('install', event => {
    self.skipWaiting();

    const cache$ = caches
        .open(CACHE_NAME)
        .then(cache => cache.addAll(URLS_TO_CACHE));

    event.waitUntil(cache$);
});

self.addEventListener('activate', event => {
    const cache$ = caches
        .keys()
        .then(cacheNames =>
            Promise.all(
                cacheNames.map(cacheName =>
                    cacheName.indexOf(CACHE_NAME) === 0
                        ? null
                        : caches.delete(cacheName)
                )
            )
        );

    event.waitUntil(cache$);
});

self.addEventListener('fetch', event => {
    const request = event.request;

    let notFetchable = false;

    BLACKLIST_ROUTES.forEach(entry => {
        if (request.url.indexOf(entry) !== -1) {
            notFetchable = true;
        }
    });

    if (notFetchable) {
        return false;
    }

    if (
        request.mode === 'navigate' &&
        request.method === 'GET' &&
        request.isHistoryNavigation
    ) {
        event.respondWith(
            fetch(request.url, {
                headers: {
                    'Cache-Control': 'no-store'
                }
            })
        );
    }

    if (
        event.request.mode === 'navigate' ||
        (event.request.method === 'GET' &&
            event.request.headers.get('accept').includes('text/html'))
    ) {
        event.respondWith(
            fetch(event.request.url).catch(() => caches.match(OFFLINE_URL))
        );
    } else {
        if (!CACHE_REGEX.test(request.url) || request.method !== 'GET') {
            return;
        }

        const requestUrl = new URL(request.url);
        if (requestUrl.origin !== location.origin) {
            return;
        }

        event.respondWith(
            caches.match(event.request).then(cached => {
                const networked = fetch(event.request).then(response => {
                    let cacheCopy = response.clone();

                    caches
                        .open(CACHE_NAME)
                        .then(cache => cache.put(event.request, cacheCopy));

                    return response;
                });

                return cached || networked;
            })
        );
    }
});
